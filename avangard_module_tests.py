#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Copyright 2019 Andrey Skvortsov <andrej.skvortzov@gmail.com>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

"""
Module implements tests for I/O modules
(digital input/output, analog input, analog output)
created by Avangard JSC.
"""

from __future__ import absolute_import
from __future__ import division
from threading import Thread, Lock
import time
import logging

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder


class MBCommunicationError(RuntimeError):
    """Exception raised in case of any communication problem"""
    def __init__(self):
        RuntimeError.__init__(self)


def modbus_check_error(req):
    """
    Helper function to raise exception
    on error in Modbus communication

    @param: req - response from pymodbus request
    """
    if req.isError():
        raise MBCommunicationError


MB_CMD_CODE_RESET = 3
MB_CMD_CODE_RTC_READ = 4
MB_CMD_CODE_RTC_WRITE = 5
MB_CMD_CODE_SW_HW_INFO = 10
MB_CMD_CODE_TEST_EEPROM_START = 11
MB_CMD_CODE_TEST_EEPROM_STATUS = 12
MB_CMD_CODE_TEST_POWER_GOOD_START = 13
MB_CMD_CODE_TEST_POWER_GOOD_STATUS = 14

TEST_POWER_GOOD_UNDEF = 0
TEST_POWER_GOOD_IN_PROGRESS = 1


MB_DEV_DIO = 1
MB_DEV_AO = 2
MB_DEV_AI = 3
MB_DEV_CPU = 4


class ModbusDeviceBase(object):
    """
    Base class to test Modbus devices.
    It contains common logic for all IO-modules, like
    initialization and RTC tests
    """

    def __init__(self, unit, device_id):
        """
        Constructor
        @param unit: Modbus device id. Integer 1-247.
        @param device_id: integer, ID to identify I/O module type.
        """
        self.unit = unit
        self.lock = Lock()
        self.inited = False
        self.device_id = device_id

        # default configuration init
        self.inpt_rw_reg = 16384
        self.hw_id_reg = 1
        self.cmd_reg = self.inpt_rw_reg + 7
        self.cmd_data_reg = self.cmd_reg + 1
        self.rtc_notify_callback = None
        self.rtc_new_value = None
        self.eeprom_notify_callback = None
        self.eeprom_start = False
        self.eeprom_test_status = True
        self.power_good_notify_callback = None
        self.power_good_start = False
        self.power_good_test_status = True

    def set_rtc_notify_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.rtc_notify_callback = callback

    def set_eeprom_notify_callback(self, callback):
        """
        Sets callback to get notification when eeprom test state is updated
        @param callback: callback function
        """
        self.eeprom_notify_callback = callback

    def start_eeprom_test(self):
        """
        Set flag to start eeprom test
        """
        self.lock.acquire()
        self.eeprom_start = True
        self.lock.release()

    def modbus_start_eeprom_test(self, client):
        """
        Blocking function to start EEPROM test
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited or not self.eeprom_start:
            return False

        req = client.write_register(self.cmd_reg, MB_CMD_CODE_TEST_EEPROM_START, unit=self.unit)
        modbus_check_error(req)

        self.lock.acquire()
        self.eeprom_start = False
        self.lock.release()
        self.eeprom_test_status = False

        return True

    def modbus_get_eeprom_test(self, client):
        """
        Blocking function to get EEPROM test results
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited or self.eeprom_test_status:
            return False

        req = client.write_register(self.cmd_reg, MB_CMD_CODE_TEST_EEPROM_STATUS, unit=self.unit)
        modbus_check_error(req)

        time.sleep(0.1)

        req = client.read_holding_registers(self.cmd_data_reg, 1, unit=self.unit)
        modbus_check_error(req)

        state = req.registers[0]
        if state > 0x8000:
            self.eeprom_test_status = True

        if self.eeprom_notify_callback is not None:
            self.eeprom_notify_callback(state)
        return True

    def set_power_good_notify_callback(self, callback):
        """
        Sets callback to get notification when power_good test state is updated
        @param callback: callback function
        """
        self.power_good_notify_callback = callback

    def start_power_good_test(self):
        """
        Set flag to start power_good test
        """
        self.lock.acquire()
        self.power_good_start = True
        self.lock.release()

    def modbus_start_power_good_test(self, client):
        """
        Blocking function to start power good test
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited or not self.power_good_start:
            return False

        req = client.write_register(self.cmd_reg, MB_CMD_CODE_TEST_POWER_GOOD_START, unit=self.unit)
        modbus_check_error(req)

        self.lock.acquire()
        self.power_good_start = False
        self.lock.release()
        self.power_good_test_status = False

        return True

    def modbus_get_power_good_test(self, client):
        """
        Blocking function to get power good test results
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited or self.power_good_test_status:
            return False

        req = client.write_register(self.cmd_reg, MB_CMD_CODE_TEST_POWER_GOOD_STATUS, unit=self.unit)
        modbus_check_error(req)

        time.sleep(0.1)

        req = client.read_holding_registers(self.cmd_data_reg, 1, unit=self.unit)
        modbus_check_error(req)

        state = req.registers[0]
        if state > TEST_POWER_GOOD_IN_PROGRESS:
            self.power_good_test_status = True

        if self.power_good_notify_callback is not None:
            self.power_good_notify_callback(state)
        return True

    # pylint: disable=unused-argument
    def modbus_dev_init(self, client):
        """
        Function that is used to set initial settings for the device
        @return bool: returns whether initialization was successful
        """
        self.inited = True
        return True

    def modbus_dev_reset(self, client):
        """
        Blocking function to reset Modbus device.
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        req = client.write_register(self.cmd_reg, MB_CMD_CODE_RESET, unit=self.unit)
        modbus_check_error(req)
        return True

    def modbus_get_rtc(self, client):
        """
        Blocking function to read RTC value from Modbus device.
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited:
            return False

        req = client.write_register(self.cmd_reg, MB_CMD_CODE_RTC_READ, unit=self.unit)
        modbus_check_error(req)

        time.sleep(0.1)

        req = client.read_holding_registers(self.cmd_data_reg, 2, unit=self.unit)
        modbus_check_error(req)
        decoder = BinaryPayloadDecoder.fromRegisters(req.registers,
                                                     byteorder=Endian.Big,
                                                     wordorder=Endian.Little)
        rtc = decoder.decode_32bit_int()
        if self.rtc_notify_callback is not None:
            self.rtc_notify_callback(rtc)
        return True

    def modbus_set_rtc(self, client):
        """
        Blocking function to write RTC value from PC to Modbus device.
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited or self.rtc_new_value is None:
            return False

        self.lock.acquire()
        rtc = self.rtc_new_value
        self.lock.release()

        builder = BinaryPayloadBuilder(wordorder=Endian.Little, byteorder=Endian.Big)
        builder.add_32bit_int(rtc)

        payload = [MB_CMD_CODE_RTC_WRITE]
        payload.extend(builder.to_registers())
        req = client.write_registers(self.cmd_reg, payload, unit=self.unit)
        modbus_check_error(req)

        self.lock.acquire()
        self.rtc_new_value = None
        self.lock.release()

        return True

    def set_rtc(self, new_rtc):
        """
        Set RTC in the device that would be written later by worker thread.
        It should be safe to call this function from wx thread.
        """
        self.lock.acquire()
        self.rtc_new_value = new_rtc
        self.lock.release()

    # pylint: disable=unused-argument,no-self-use
    def modbus_reset_to_default(self, client):
        """
        Function resets device settings to defaults.
        Function blocks.
        It's should be called after all tests are done to reset device to initial state.
        @return bool: True if reset was successful
        """
        return True

    def get_test_functions(self):
        """
        Returns list of functions that has to be called
        by Modbus worker for doing all tests
        """
        return [
            self.modbus_dev_init,
            self.modbus_start_eeprom_test,
            self.modbus_get_eeprom_test,
            self.modbus_start_power_good_test,
            self.modbus_get_power_good_test,
            self.modbus_get_rtc,
            self.modbus_set_rtc
        ]


class ModbusDeviceAO(ModbusDeviceBase):
    """
    Class to test Analog Output Modbus module.
    """

    def __init__(self, unit):
        """
        Constructor
        @param unit: Modbus device addr, integer 1-247
        """
        ModbusDeviceBase.__init__(self, unit, MB_DEV_AO)
        self.aout = 0.0
        self.aout_reg = self.inpt_rw_reg + 32
        self.ch_num = 6

    def set_ao_value(self, value):
        """
        Set output current for analog output that would be written later by worker thread.
        It should be safe to call this function from wx thread.
        """
        aout_min = 0.0
        aout_max = 20.0
        value = max(aout_min, min(value, aout_max))
        self.lock.acquire()
        self.aout = value/aout_max
        self.lock.release()

    def modbus_set_ao_value(self, client):
        """
        Blocking function to write output current value to Modbus device.
        Never call this function from wx thread.
        @return bool: True if communication with hw was successful
        """
        if not self.inited:
            return False

        self.lock.acquire()
        val = self.aout
        self.lock.release()
        builder = BinaryPayloadBuilder(wordorder=Endian.Big, byteorder=Endian.Big)
        builder.add_32bit_float(val)
        payload = builder.to_registers()
        fullpayload = payload*self.ch_num
        req = client.write_registers(self.aout_reg, fullpayload, unit=self.unit)
        modbus_check_error(req)
        return True

    def get_test_functions(self):
        """
        Returns list of functions that has to be called
        by Modbus worker for doing all tests
        """
        func = ModbusDeviceBase.get_test_functions(self)
        func.append(self.modbus_set_ao_value)
        return func


class ModbusDeviceDIO(ModbusDeviceBase):
    """
    Class to test Analog Output Modbus module.
    """

    def __init__(self, unit):
        """
        Constructor
        @param unit: Modbus device addr, integer 1-247
        """
        ModbusDeviceBase.__init__(self, unit, MB_DEV_DIO)
        self.do_val = 0
        self.input_mode = True
        self.dio_set_reg = 5
        self.di_reg = self.inpt_rw_reg + 32
        self.do_reg = self.inpt_rw_reg + 33
        self.di_notify_callback = None
        self.do_notify_callback = None

    def set_di_notify_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.di_notify_callback = callback

    def set_do_notify_callback(self, callback):
        """
        Sets callback to get notification if new output data are updated in hw
        @param callback: callback function
        """
        self.do_notify_callback = callback

    def set_do_value(self, value):
        """
        Sets requested state for digital outputs.
        It should be safe to call this function from wx thread.
        """
        self.lock.acquire()
        self.do_val = value
        self.lock.release()

    def set_di_input_mode(self, value):
        """
        Sets whether all I/O of the module should work as inputs.
        @param value: True  - all I/O should be configured as inputs,
                      False - all I/O should be configured as outputs,
        It should be safe to call this function from wx thread.
        """
        self.lock.acquire()
        self.input_mode = value
        self.inited = False
        self.lock.release()

    def modbus_dev_init(self, client):
        """
        Function initialize module according to previously set input mode and
        resets device to apply new settings.
        @return bool: returns True if communication with hw was successful
        """
        if self.inited:
            return False

        dio_cfg_input = 0
        dio_cfg_output = 1
        payload = [dio_cfg_input if self.input_mode else dio_cfg_output]*16
        req = client.write_registers(self.dio_set_reg, payload, unit=self.unit)
        modbus_check_error(req)

        # settings are applied only after reset
        time.sleep(0.2)
        self.modbus_dev_reset(client)
        time.sleep(0.2)
        self.inited = True
        return True

    def modbus_set_do_value(self, client):
        """
        Blocking function to write states for digital outputs to Modbus device.
        Never call this function from wx thread.
        @return bool: returns True if communication with hw was successful
        """
        if (not self.inited) or self.input_mode:
            return False

        self.lock.acquire()
        val = self.do_val
        self.lock.release()
        if val is None:
            return False

        req = client.write_registers(self.do_reg, val, unit=self.unit)
        modbus_check_error(req)

        self.lock.acquire()
        if val == self.do_val:
            self.do_val = None
        self.lock.release()

        if self.do_notify_callback is not None:
            self.do_notify_callback(val)
        return True

    def modbus_get_di_value(self, client):
        """
        Blocking function reads current state of digital inputs from Modbus device.
        Never call this function from wx thread.
        @return bool: returns True if communication with hw was successful
        """
        if (not self.inited) or (not self.input_mode):
            return False

        req = client.read_holding_registers(self.di_reg, 1, unit=self.unit)
        modbus_check_error(req)

        din = req.registers[0]
        if self.di_notify_callback is not None:
            self.di_notify_callback(din)
        return True

    def modbus_reset_to_default(self, client):
        """
        Function resets device settings to defaults.
        Default state is all I/O are configured as inputs.
        Function blocks.
        @return: bool. If reset was successful.
        """
        self.set_di_input_mode(True)
        return self.modbus_dev_init(client)

    def get_test_functions(self):
        """
        Returns list of functions that has to be called
        by Modbus worker for doing all tests
        """
        func = ModbusDeviceBase.get_test_functions(self)
        func.append(self.modbus_set_do_value)
        func.append(self.modbus_get_di_value)
        return func


# define constans for all supported sensor types
[
    SENSOR_TYPE_OFF,
    SENSOR_TYPE_CU50,
    SENSOR_TYPE_CU100,
    SENSOR_TYPE_50M,
    SENSOR_TYPE_100M,
    SENSOR_TYPE_PT50,
    SENSOR_TYPE_PT100,
    SENSOR_TYPE_PT500,
    SENSOR_TYPE_PT1000,
    SENSOR_TYPE_50P,
    SENSOR_TYPE_100P,
    SENSOR_TYPE_500P,
    SENSOR_TYPE_100N,

    SENSOR_TYPE_PT50_LIM,
    SENSOR_TYPE_PT100_LIM,
    SENSOR_TYPE_PT500_LIM,
    SENSOR_TYPE_PT1000_LIM,
    SENSOR_TYPE_50P_LIM,
    SENSOR_TYPE_100P_LIM,
    SENSOR_TYPE_500P_LIM,

    SENSOR_TYPE_R0_100,
    SENSOR_TYPE_R0_500,
    SENSOR_TYPE_R0_1000,
    SENSOR_TYPE_R0_2000,
    SENSOR_TYPE_R0_3000,

    SENSOR_TYPE_U0_3000,
    SENSOR_TYPE_U0_750,
] = range(27)


class ModbusDeviceAI(ModbusDeviceBase):
    """
    Class to test Analog inputs Modbus module.
    """
    ai_supported_sensors = {
        SENSOR_TYPE_OFF:        (0.0, 1.0,    u'Выключен'),
        SENSOR_TYPE_CU50:       (0.0, 1.0,    u'Cu50 [-50:200] °C'),
        SENSOR_TYPE_CU100:      (0.0, 1.0,    u'Cu100 [-50:200] °C'),
        SENSOR_TYPE_50M:        (0.0, 1.0,    u'50M [-180:200] °C'),
        SENSOR_TYPE_100M:       (0.0, 1.0,    u'100M [-180:200] °C'),
        SENSOR_TYPE_PT50:       (0.0, 1.0,    u'Pt50 [-200 : 850] °C'),
        SENSOR_TYPE_PT100:      (0.0, 1.0,    u'Pt100 [-200 : 850] °C'),
        SENSOR_TYPE_PT500:      (0.0, 1.0,    u'Pt500 [-200 : 850] °C'),
        SENSOR_TYPE_PT1000:     (0.0, 1.0,    u'Pt1000 [-200 : 850] °C'),
        SENSOR_TYPE_50P:        (0.0, 1.0,    u'50П [-200 : 850] °C'),
        SENSOR_TYPE_100P:       (0.0, 1.0,    u'100П [-200 : 850] °C'),
        SENSOR_TYPE_500P:       (0.0, 1.0,    u'500П [-200 : 850] °C'),
        SENSOR_TYPE_100N:       (0.0, 1.0,    u'100Н [-60 : 180] °C'),

        SENSOR_TYPE_PT50_LIM:   (0.0, 1.0,    u'Pt50 [-200:200] °C'),
        SENSOR_TYPE_PT100_LIM:  (0.0, 1.0,    u'Pt100 [-200 : 200] °C'),
        SENSOR_TYPE_PT500_LIM:  (0.0, 1.0,    u'Pt500 [-200 : 200] °C'),
        SENSOR_TYPE_PT1000_LIM: (0.0, 1.0,    u'Pt1000 [-200 : 200] °C'),
        SENSOR_TYPE_50P_LIM:    (0.0, 1.0,    u'50П [-200 : 200] °C'),
        SENSOR_TYPE_100P_LIM:   (0.0, 1.0,    u'100П [-200 : 200] °C'),
        SENSOR_TYPE_500P_LIM:   (0.0, 1.0,    u'500П [-200 : 200] °C'),

        SENSOR_TYPE_R0_100:     (0.0, 100.0,  u'R [0 : 100] Ом'),
        SENSOR_TYPE_R0_500:     (0.0, 500.0,  u'R [0 : 500] Ом'),
        SENSOR_TYPE_R0_1000:    (0.0, 1000.0, u'R [0 : 1000] Ом'),
        SENSOR_TYPE_R0_2000:    (0.0, 2000.0, u'R [0 : 2000] Ом'),
        SENSOR_TYPE_R0_3000:    (0.0, 3000.0, u'R [0 : 3000] Ом'),

        SENSOR_TYPE_U0_3000:    (0.0, 3.0,    u'U [0 : 3] В'),
        SENSOR_TYPE_U0_750:     (0.0, 0.750,  u'U [0 : 750] мВ'),
    }

    def __init__(self, unit):
        """
        Constructor
        @param unit: Modbus device addr, integer 1-247
        """
        ModbusDeviceBase.__init__(self, unit, MB_DEV_AI)
        self.ai_type = 0
        self.ai_get_reg = 32
        self.ai_set_reg = 5
        self.ai_min_reg = 13
        self.ai_max_reg = 29
        self.ai_notify_callback = None

    def set_ai_notify_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.ai_notify_callback = callback

    def set_ai_input_mode(self, val):
        """
        Selects sensor type for all input channels.
        @param val: integer id for sensor type (see constans before class definitions)
        It should be safe to call this function from wx thread.
        """
        self.lock.acquire()
        if self.ai_type != val:
            self.ai_type = val
            self.inited = False
        self.lock.release()

    def modbus_dev_init(self, client):
        """
        Function initialize module according to previously set sensor type and
        resets device to apply new settings.
        @return bool: returns True if communication with hw was successful
        """
        if self.inited:
            return False

        ai_min = 0.0
        ai_max = ModbusDeviceAI.ai_supported_sensors[self.ai_type][1]

        self.lock.acquire()
        payload = []
        payload.extend([self.ai_type]*8)

        builder = BinaryPayloadBuilder(wordorder=Endian.Big, byteorder=Endian.Big)
        builder.add_32bit_float(ai_min)
        ai_min_payload = builder.to_registers()
        payload.extend(ai_min_payload*8)

        builder = BinaryPayloadBuilder(wordorder=Endian.Big, byteorder=Endian.Big)
        builder.add_32bit_float(ai_max)
        ai_max_payload = builder.to_registers()
        payload.extend(ai_max_payload*8)
        self.lock.release()

        req = client.write_registers(self.ai_set_reg, payload, unit=self.unit)
        modbus_check_error(req)

        # settings are applied only after reset
        time.sleep(0.2)
        self.modbus_dev_reset(client)
        time.sleep(2.0)
        self.inited = True
        return True

    def modbus_get_ai_value(self, client):
        """
        Blocking function to read current measured values and sensor states.
        Never call this function from wx thread.
        @return bool: returns True if communication with hw was successful
        """
        if not self.inited:
            return False

        chs = 8
        req = client.read_input_registers(self.ai_get_reg, (2 + 1) * chs, unit=self.unit)
        modbus_check_error(req)

        ains = []
        for chn in range(chs):
            regs = req.registers[chn*2: chn*2 + 2]
            decoder = BinaryPayloadDecoder.fromRegisters(regs,
                                                         byteorder=Endian.Big,
                                                         wordorder=Endian.Big)
            val = {
                "value": decoder.decode_32bit_float(),
                "state": req.registers[chs*2 + chn]
            }
            ains.append(val)

        if self.ai_notify_callback is not None:
            self.ai_notify_callback(ains)
        return True

    def modbus_reset_to_default(self, client):
        """
        Function resets device settings to defaults.
        Function blocks.
        @return bool: returns True if communication with hw was successful
        """
        self.set_ai_input_mode(SENSOR_TYPE_OFF)
        return self.modbus_dev_init(client)

    def get_test_functions(self):
        """
        Returns list of functions that has to be called
        by Modbus worker for doing all tests
        """
        func = ModbusDeviceBase.get_test_functions(self)
        func.append(self.modbus_get_ai_value)
        return func


class ModbusDeviceCPU(ModbusDeviceBase):
    """
    Class to test PLC-CPU.
    """

    def __init__(self, unit):
        """
        Constructor
        @param unit: Modbus device addr, integer 1-247
        """
        ModbusDeviceBase.__init__(self, unit, MB_DEV_CPU)


class ModbusThreadedClient(object):
    """
    Controls worker thread communicated with hardware to allow non-blocking UI.
    """

    def __init__(self, port='/dev/ttyUSB0'):
        """
        Constructor
        @param dev_type: device class, derived from ModbusDeviceBase
        @param port: string with serial port device is connected to. Default '/dev/ttyUSB0'
        """
        self.thread = None
        self.stop_thread = False
        self.port = port
        self.client = None
        self.unit = 1
        self.dev = None
        self.log = None
        self.status_message_callback = None
        self.log_message_callback = None
        self.thread_stop_callback = None
        self.thread_start_callback = None

    def set_status_message_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.status_message_callback = callback

    def set_log_message_callback(self, callback):
        """
        Sets callback to add new entry into application log
        @param callback: callback function
        """
        self.log_message_callback = callback

    def set_thread_stop_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.thread_stop_callback = callback

    def set_thread_start_callback(self, callback):
        """
        Sets callback to get notification if data are updated
        @param callback: callback function
        """
        self.thread_start_callback = callback

    def set_status_message(self, message):
        """
        Send updated status to UI wx thread.
        Thread safe function.
        """
        if self.status_message_callback is not None:
            self.status_message_callback(message)

    def reset_dev_to_defaults(self):
        """
        Reliably resets device settings to defaults.
        Never call this function from wx thread.
        """
        tries = 3
        for dummy in range(tries):
            try:
                self.dev.modbus_reset_to_default(self.client)
                return True
            except MBCommunicationError:
                pass
        self.set_status_message(u"Не удалось сбросить настройки устройства в исходные")
        return False

    def modbus_get_device_id(self):
        """
        Blocking function to get Modbus device.
        Never call this function from wx thread.
        @return: integer device id
        """
        hw_id_reg = 1
        req = self.client.read_input_registers(hw_id_reg, 1, unit=self.unit)
        modbus_check_error(req)
        device_id = req.registers[0]
        return device_id

    def get_device_id(self):
        """
        Ask connected device for its type (up to tree attempts are made)
        @return int: returns device id or None if no device is connected
        """
        tries = 3
        for dummy in range(tries):
            try:
                device_id = self.modbus_get_device_id()
            except MBCommunicationError:
                continue
            return device_id
        return None

    def thread_main(self):
        """
        Main worker thread to communicate with Modbus device.
        Don't call this function from wx thread.
        """
        self.log = logging.getLogger('pymodbus.transaction')
        self.log.setLevel(logging.DEBUG)

        class ProgressConsoleHandler(logging.Handler):
            def emit(self2, record):
                msg = self2.format(record)
                self.log_message_callback(msg)

        handler = ProgressConsoleHandler()
        formatter = logging.Formatter('%(asctime)s: %(message)s')
        handler.setFormatter(formatter)
        self.log.addHandler(handler)

        self.worker()
        if self.thread_stop_callback:
            self.thread_stop_callback()

    def worker(self):
        """
        Main worker thread to communicate with Modbus device.
        Don't call this function from wx thread.
        """
        try:
            self.client = ModbusClient(method='rtu', port=self.port, timeout=2, baudrate=9600)
            self.client.inter_char_timeout = 0.05
            self.client.connect()
            self.set_status_message(u"Порт открыт. Настройка прибора.")
        except Exception:
            self.set_status_message(u"Не удалось открыть порт")
            self.client.close()
            return

        dev = self.get_device_id()
        if dev is None:
            self.set_status_message(u'Прибор не подключён')
            self.client.close()
            return

        devices = {
            MB_DEV_DIO: ModbusDeviceDIO,
            MB_DEV_AO: ModbusDeviceAO,
            MB_DEV_AI: ModbusDeviceAI,
            MB_DEV_CPU: ModbusDeviceCPU,
        }
        try:
            self.dev = devices[dev](unit=self.unit)
        except KeyError:
            self.set_status_message(u'Неподдерживаемое устройство')
            self.client.close()
            return

        self.set_status_message(u'Соединение с прибором успешно установлено')

        if self.thread_start_callback:
            self.thread_start_callback()

        test_funcs = self.dev.get_test_functions()
        while not self.stop_thread:
            time.sleep(0.05)
            for test in test_funcs:
                pause = True
                try:
                    if test(self.client):
                        self.set_status_message(u"Опрос прибора")
                    else:
                        pause = False
                except MBCommunicationError:
                    self.set_status_message(u"Ошибка опроса прибора")

                if pause:
                    time.sleep(0.05)

        no_err = self.reset_dev_to_defaults()
        self.client.close()
        if no_err:
            self.set_status_message(u"Порт успешно закрыт")

    def start(self):
        """
        Creates worker thread.
        """
        self.thread = Thread(target=self.thread_main)
        self.thread.start()

    def stop(self):
        """
        Stops worker thread.
        """
        if self.thread:
            self.stop_thread = True
