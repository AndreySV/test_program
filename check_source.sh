#!/bin/sh
# -*- coding: utf-8 -*-
#
#  Copyright 2019 Andrey Skvortsov <andrej.skvortzov@gmail.com>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

exit_code=0
set_exit_error()
{
    if [ $exit_code -eq 0 ]; then
       exit_code=1
    fi
}

version_gt()
{
    test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1";
}


compile_checks()
{
    echo "Syntax checking using python ..."
    python --version

    # remove compiled Python files
    find . -name '*.pyc' -exec rm -f {} \;

    for i in $py_files; do
        # echo $i
        python -m py_compile $i
        if [ $? -ne 0 ]; then
            echo "Syntax error in $i"
            set_exit_error
        fi
    done
    echo "DONE"
    echo ""
}


python3_compile_checks()
{
    echo "Syntax checking using python3 ..."
    python3 --version

    # remove compiled Python files
    find . -name '*.pyc' -exec rm -f {} \;

    for i in $py_files; do
        # echo $i
        python3 -m py_compile $i
        if [ $? -ne 0 ]; then
            echo "Syntax error in $i"
            set_exit_error
        fi
    done

    # remove compiled Python files
    find . -name '*.pyc' -exec rm -f {} \;

    echo "DONE"
    echo ""
}

localization_checks()
{
    echo "Check correct localization formats"
    xgettext --version
    
    for i in $py_files; do
        xgettext -s --language=Python --package-name Beremiz --output=/tmp/m.pot $i 2>&1 | grep 'warning'
        if [ $? -eq 0 ]; then
            echo "Syntax error in $i"
            set_exit_error
        fi
    done
    echo "DONE"
    echo ""
}

# pep8 was renamed to pycodestyle
# detect existed version
pep8_detect()
{
    test -n $pep8 && (which pep8 > /dev/null) && pep8="pep8"
    test -n $pep8 && (which pycodestyle > /dev/null) && pep8="pycodestyle"
    if [ -z $pep8 ]; then
        echo "pep8/pycodestyle is not found"
        set_exit_error
    fi
    echo -n "pep8 version: "
    $pep8 --version
}

pep8_checks_default()
{
    echo "Check basic code-style problems for PEP-8"

    test -n $pep8 && pep8_detect
    test -z $pep8 && return

    user_ignore=
    user_ignore=$user_ignore,W606  # W606 'async' and 'await' are reserved keywords starting with Python 3.7

    # ignored by default,
    default_ignore=
    default_ignore=$default_ignore,E121  # E121 continuation line under-indented for hanging indent
    default_ignore=$default_ignore,E123  # E123 closing bracket does not match indentation of opening bracket’s line
    default_ignore=$default_ignore,E126  # E126 continuation line over-indented for hanging indent
    default_ignore=$default_ignore,E133  # E133 closing bracket is missing indentation
    default_ignore=$default_ignore,E226  # E226 missing whitespace around arithmetic operator
    default_ignore=$default_ignore,E241  # E241 multiple spaces after ':'
    default_ignore=$default_ignore,E242  # E242 tab after ‘,’
    default_ignore=$default_ignore,E704  # E704 multiple statements on one line (def)
    default_ignore=$default_ignore,W503  # W503 line break occurred before a binary operator
    default_ignore=$default_ignore,W504  # W504 line break occurred after a binary operator
    default_ignore=$default_ignore,W505  # W505 doc line too long (82 > 79 characters)
    ignore=$user_ignore,$default_ignore

    $pep8 --max-line-length 3000 --ignore=$ignore --exclude build $py_files
    if [ $? -ne 0 ]; then
        set_exit_error
    fi

    echo "DONE"
    echo ""
}


pep8_checks_selected()
{
    echo "Check basic code-style problems for PEP-8 (selective)"

    test -n $pep8 && pep8_detect
    test -z $pep8 && return

    # select checks:
    user_select=
    user_select=$user_select,W291   # W291 trailing whitespace
    user_select=$user_select,E401   # E401 multiple imports on one line
    user_select=$user_select,E265   # E265 block comment should start with '# '
    user_select=$user_select,E228   # E228 missing whitespace around modulo operator
    user_select=$user_select,W293   # W293 blank line contains whitespace
    user_select=$user_select,E302   # E302 expected 2 blank lines, found 1
    user_select=$user_select,E301   # E301 expected 2 blank lines, found 1
    user_select=$user_select,E261   # E261 at least two spaces before inline comment
    user_select=$user_select,E271   # E271 multiple spaces after keyword
    user_select=$user_select,E231   # E231 missing whitespace after ','
    user_select=$user_select,E303   # E303 too many blank lines (2)
    user_select=$user_select,E225   # E225 missing whitespace around operator
    user_select=$user_select,E711   # E711 comparison to None should be 'if cond is not None:'
    user_select=$user_select,E251   # E251 unexpected spaces around keyword / parameter equals
    user_select=$user_select,E227   # E227 missing whitespace around bitwise or shift operator
    user_select=$user_select,E202   # E202 whitespace before ')'
    user_select=$user_select,E201   # E201 whitespace after '{'
    user_select=$user_select,W391   # W391 blank line at end of file
    user_select=$user_select,E305   # E305 expected 2 blank lines after class or function definition, found X
    user_select=$user_select,E306   # E306 expected 1 blank line before a nested definition, found X
    user_select=$user_select,E703   # E703 statement ends with a semicolon
    user_select=$user_select,E701   # E701 multiple statements on one line (colon)
    user_select=$user_select,E221   # E221 multiple spaces before operator
    user_select=$user_select,E741   # E741 ambiguous variable name 'l'
    user_select=$user_select,E111   # E111 indentation is not a multiple of four
    user_select=$user_select,E222   # E222 multiple spaces after operator
    user_select=$user_select,E712   # E712 comparison to True should be 'if cond is True:' or 'if cond:'
    user_select=$user_select,E262   # E262 inline comment should start with '# '
    user_select=$user_select,E203   # E203 whitespace before ','
    user_select=$user_select,E731   # E731 do not assign a lambda expression, use a def
    user_select=$user_select,W601   # W601 .has_key() is deprecated, use 'in'
    user_select=$user_select,E502   # E502 the backslash is redundant between brackets
    user_select=$user_select,W602   # W602 deprecated form of raising exception
    user_select=$user_select,E129   # E129 visually indented line with same indent as next logical line
    user_select=$user_select,E127   # E127 continuation line over-indented for visual indent
    user_select=$user_select,E128   # E128 continuation line under-indented for visual indent
    user_select=$user_select,E125   # E125 continuation line with same indent as next logical line
    user_select=$user_select,E114   # E114 indentation is not a multiple of four (comment)
    user_select=$user_select,E211   # E211 whitespace before '['
    user_select=$user_select,W191   # W191 indentation contains tabs
    user_select=$user_select,E101   # E101 indentation contains mixed spaces and tabs
    user_select=$user_select,E124   # E124 closing bracket does not match visual indentation
    user_select=$user_select,E272   # E272 multiple spaces before keyword
    user_select=$user_select,E713   # E713 test for membership should be 'not in'
    user_select=$user_select,E122   # E122 continuation line missing indentation or outdented
    user_select=$user_select,E131   # E131 continuation line unaligned for hanging indent
    user_select=$user_select,E721   # E721 do not compare types, use 'isinstance()'
    user_select=$user_select,E115   # E115 expected an indented block (comment)
    user_select=$user_select,E722   # E722 do not use bare except'
    user_select=$user_select,E266   # E266 too many leading '#' for block comment
    user_select=$user_select,E402   # E402 module level import not at top of file
    user_select=$user_select,W503   # W503 line break before binary operator

    $pep8 --select $user_select --exclude=build $py_files
    if [ $? -ne 0 ]; then
        set_exit_error
    fi

    echo "DONE"
    echo ""
}

flake8_checks()
{
    echo "Check for problems using flake8 ..."

    which flake8 > /dev/null
    if [ $? -ne 0 ]; then
        echo "flake8 is not found"
        set_exit_error
        return
    fi

    echo -n "flake8 version: "
    flake8 --version

    flake8 --max-line-length=300  --exclude=build --builtins="_" $py_files
    if [ $? -ne 0 ]; then
        set_exit_error
    fi

    echo "DONE"
    echo ""
}



pylint_exist_check()
{
    pylint_found="yes"
    which $PYLINT  > /dev/null
    if [ $? -ne 0 ]; then
        echo "$PYLINT is not found"
        set_exit_error
	pylint_found=""
        return
    fi
    $PYLINT --version
    
    options=
    options="$options --rcfile=.pylint"
}

pylint_run_checks()
{
    ver=$($PYLINT --version 2>&1 | grep $PYLINT  | awk '{ print $2 }')
    if version_gt $ver '1.6.8'; then
	echo "Use multiple threads for $PYLINT"
	options="$options --jobs=0 "
    fi
    
    echo $options

    echo $py_files | xargs $PYLINT $options
    if [ $? -ne 0 ]; then
        set_exit_error
    fi

    echo "DONE"
    echo ""
}

pylint_python3_checks()
{
    echo "Check for python3 problems using pylint ..."
    PYLINT=pylint
    pylint_exist_check
    [ -z $pylint_found ] && return

    options="$options --py3k"   # report errors for Python 3 porting
    pylint_run_checks
}


pylint_checks()
{
    echo "Check for problems using pylint ..."
    pylint_exist_check
    [ -z $pylint_found ] && return
    
    
    disable=
    # These warnings most likely will not be fixed

    # disable=$disable,C0103        # invalid-name
    disable=$disable,C0326        # bad whitespace
    disable=$disable,W0703        # broad-except
    disable=$disable,C0301        # Line too long
    disable=$disable,R0902        # (too-many-instance-attributes) Too many instance attributes (10/7)
    disable=$disable,R0904        # (too-many-public-methods) Too many public methods (41/20)
    disable=$disable,R0914        # (too-many-locals) Too many local variables (18/15)
    disable=$disable,R0915        # (too-many-statements) Too many statements (57/50)
    disable=$disable,R0903        # (too-few-public-methods) Too few public methods (1/2)
    disable=$disable,R0901        # (too-many-ancestors) Too many ancestors (8/7)

    disable=$disable,R0205        # (useless-object-inheritance)] X inherits from object, can be safely removed from bases in python3
    enable=

    
    if [ -n "$enable" ]; then
        options="$options --disable=all"
        options="$options --enable=$enable"
    else
        options="$options --disable=$disable"
    fi
    pylint_run_checks
}

pylint2_checks()
{
    PYLINT=pylint
    pylint_checks
}

pylint3_checks()
{
    PYLINT=pylint3
    pylint_checks
    PYLINT=pylint
}

get_files_to_check_hg()
{
    if [ "$1" = "--only-changes" ]; then
        if which hg > /dev/null; then
            if [ ! -z "$HG_NODE" ]; then
                hg_change="--change $HG_NODE"
                msg="for commit $HG_NODE"
            else
                hg_change=""
                msg="in local repository"
            fi
            echo "Only changes ${msg} will be checked"
            echo ""
            py_files=$(hg status -m -a -n -I '**.py' $hg_change)
            if [ $? -ne 0 ]; then
                exit 1;
            fi
       fi
    fi    
}

get_files_to_check()
{
    py_files=$(find . -name '*.py' -not -path '*/build/*')
    # py_files=$(echo $py_files | sed  's/\.\/Test.py//')
    echo $py_files
    if [ -e .hg/skiphook ]; then
	echo "Skipping checks in the hook ..."
	exit 0
    fi
    get_files_to_check_hg $0
    if [ "$1" = "--files-to-check" ]; then
        list="$2"
        if [ -z "$list" ]; then
            echo "--files-to-check requires filename as argument"
            print_help
        fi
        if [ -e "$list" ]; then
            py_files=$(cat $2 | grep '\.py$')
        fi
    fi
    if [ -z "$py_files" ]; then
        echo "No files to check"
        exit 0;
    fi
}


print_help()
{
    echo "Usage: check_source.sh [--only-changes | --files-to-check <filename> ]"
    echo ""
    echo "By default without arguments script checks all python source files"
    echo ""
    echo "--only-changes"
    echo "                only files with local changes are checked. "
    echo "                If script is called from mercurial pretxncommit hook,"
    echo "                then only commited files are checked"
    echo ""
    echo "--files-to-check <file.lst>"
    echo "                script read list of files to check from file.lst"

    exit 1
}

main()
{
    get_files_to_check $@
    python3_compile_checks
    compile_checks
    localization_checks
    pep8_checks_default
    # pep8_checks_selected

    # flake8_checks
    pylint_python3_checks
    pylint2_checks
    pylint3_checks
    exit $exit_code
}

[ "$1" = "--help" -o "$1" = "-h" ] && print_help
main $@
